package com.seidor.port.inbound.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seidor.application.SpringBootSeidorApplication;
import com.seidor.port.inbound.api.dto.BuildDto;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootSeidorApplication.class)
@AutoConfigureMockMvc
public class SpringBootSwagger2ApplicationTests {

  @Autowired
  private MockMvc mockMvc;

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void givenAnValidBuildDtoShouldCreated() throws Exception {
    BuildDto build = BuildDto.builder().buildId(1).name("build").pathRepo("/repo1/develop")
        .version("2.2.2").build();

    mockMvc
        .perform(post("/api/builds").contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(build)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  @Test
  public void givenAnInvalidBuildDtoShouldThrowError() throws Exception {
    BuildDto build = BuildDto.builder().buildId(2).name("build&&&%").pathRepo("repoSinSlash")
        .version("2.2.2").build();

    mockMvc
        .perform(post("/api/builds").contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(build)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

}
