package com.seidor.port.outbound.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.seidor.domain.aggregate.Build;

@Repository
public interface BuildRepository extends JpaRepository<Build, Long> {
  List<Build> findByVersion(String version);

  List<Build> findByNameContaining(String name);
}
