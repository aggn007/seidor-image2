package com.seidor.port.inbound.api.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class BuildDto {

  @NotNull(message = "{buildId.missing}")
//  @Size(min = 2, max = 100, message = "The length of full name must be between 2 and 100 characters.")
  private Integer buildId;

  @NotEmpty(message = "{name.missing}")
  @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "{name.notAllow}")
  private String name;
//
  @NotEmpty(message = "{pathRepo.missing}")
  @Pattern(regexp = ".*[/\\\\].*", message = "{pathRepo.notAllow}")
//  @Past(message = "The date of birth must be in the past.")
  private String pathRepo;
  
  @NotEmpty(message = "{version.missing}")
  @Pattern(regexp = "^(\\d+\\.)?(\\d+\\.)?(\\*|\\d+)$", message = "{version.notAllow}")
//  @Past(message = "The date of birth must be in the past.")
  private String version;

//  public User toUser() {
//    return new User()
//        .setName(fullName)
//        .setEmail(email.toLowerCase())
//        .setBirthDate(dateOfBirth)
//        .setGender(gender)
//        .setAddress(address.toAddress());
//  }
}