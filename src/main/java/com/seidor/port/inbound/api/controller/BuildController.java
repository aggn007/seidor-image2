package com.seidor.port.inbound.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.seidor.domain.aggregate.Build;
import com.seidor.port.inbound.api.dto.BuildDto;
import com.seidor.port.outbound.repository.BuildRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/jenkins.localhost.com")
public class BuildController {

  @Autowired
  BuildRepository buildRepository;

  @ApiOperation(value = "Get builds in the System ", response = Build.class, tags = "getBuilds")
  @ApiResponses(value = { 
      @ApiResponse(code = 200, message = "Suceess|OK"),
      @ApiResponse(code = 401, message = "not authorized!"), 
      @ApiResponse(code = 403, message = "forbidden!!!"),
      @ApiResponse(code = 404, message = "not found!!!") })
  @GetMapping("/builds")
  public ResponseEntity<List<Build>> getAllBuilds(@RequestParam(required = false) String name) {
    try {
      List<Build> builds = new ArrayList<Build>();

      if (name == null)
        buildRepository.findAll().forEach(builds::add);
      else
        buildRepository.findByNameContaining(name).forEach(builds::add);

      if (builds.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(builds, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/builds")
  public ResponseEntity<Build> createBuild(@Valid @RequestBody BuildDto buildDto) {
    try {
      Build build = buildRepository.save(new Build(buildDto.getBuildId(), buildDto.getName(),
          buildDto.getPathRepo(), buildDto.getVersion()));
      return new ResponseEntity<>(build, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
