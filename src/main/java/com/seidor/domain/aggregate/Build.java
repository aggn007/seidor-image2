package com.seidor.domain.aggregate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "builds")
@Data
public class Build {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "title")
  private Integer buildId;

  @Column(name = "name")
  private String name;

  @Column(name = "pathRepo")
  private String pathRepo;

  @Column(name = "version")
  private String version;

  public Build() {

  }

  public Build(Integer buildId, String name, String pathRepo, String version) {
    super();
    this.buildId = buildId;
    this.name = name;
    this.pathRepo = pathRepo;
    this.version = version;
  }


}
