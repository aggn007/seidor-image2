# seidor-image2

Service that  adds a new Build with this structure:
– buildId (number)
– name (string - only letters)
– pathRepo (string with slashes (\))
– version (M.m.f)


### Requeriments 

| Herramienta     | Version                           |
|:----------------|:----------------------------------|
| Java            | 11.x                              |
| Maven           |                                   |
| Lombok          | 1.x                               |

### Service context

- /seidor-demo/jenkins.localhost.com/builds

### Service details

| Endpoint                                     | Descripcion                                       |
|:---------------------------------------------|:--------------------------------------------------|
| (POST) /jenkins.localhost.com/builds                             |Register a new build
| (GET)  /jenkins.localhost.com/builds               |Return list of builds     

## Docker
Execute the following instructions:
- git clone https://gitlab.com/aggn007/seidor-image2.git
- cd seidor-image2
- mvn clean install
- docker build -t seidor-image2 .
- docker run -p 9090:8080 seidor-image2
- You can now access http://localhost:9090/seidor-demo/swagger-ui.html#!/ with a browser



